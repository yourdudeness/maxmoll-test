import 'jquery.global.js';
import fancybox from '@fancyapps/fancybox';
import page from 'page';
import forms from 'forms';
import {
  disableBodyScroll,
  enableBodyScroll,
  clearAllBodyScrollLocks
} from 'body-scroll-lock';

import Swiper from 'swiper/bundle';

import mainPage from 'index.page';


let app = {


  scrollToOffset: 200, // оффсет при скролле до элемента
  scrollToSpeed: 500, // скорость скролла

  init: function () {
    // read config
    if (typeof appConfig === 'object') {
      Object.keys(appConfig).forEach(key => {
        if (Object.prototype.hasOwnProperty.call(app, key)) {
          app[key] = appConfig[key];
        }
      });
    }

    app.currentID = 0;

    // Init page
    this.page = page;
    this.page.init.call(this);

    this.forms = forms;
    this.forms.init.call(this);

    // Init page

    this.mainPage = mainPage;
    this.mainPage.init.call(this);

    //window.jQuery = $;
    window.app = app;

    app.document.ready(() => {


    });

    // .replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 ')



    app.window.on('load', () => {




      const count = document.querySelector('.amount-item');
      let price = document.querySelector('#test');
      const btnDec = document.querySelector('.btn-dec')
      const btnInc = document.querySelector('.btn-inc')
      

      let priceCut = price.textContent.split(' ').join('').slice(1, -2)

      function test() {
        let countValue = count.value
        if (countValue <= 0) {
          return 0
        } else {
          return countValue
        }
      }

      count.addEventListener('input', () => {


        let inputTest = test();
        changeAmount(inputTest)

      })

      function changeAmount(item) {
        let result = priceCut * item;
        let strResult = String(result);
        price.innerHTML = strResult.replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 ') + ' ' + '₽';
      }


      btnDec.addEventListener('click', () => {
        function dec() {
          let countVal = --count.value
          return countVal

        }

        let decRes = dec();
        if (decRes <= 0) {
          return 0
        } else {
          changeAmount(decRes)
        }



      })

      btnInc.addEventListener('click', () => {
        function inc() {
          let countVal = ++count.value
          return countVal
        }

        let incRes = inc();
        if (incRes <= 0) {
          return 0
        } else {
          changeAmount(incRes)
        }

      })


      var swiper = new Swiper('.swiper-container', {
        pagination: {
          el: '.swiper-pagination',
          type: 'fraction',
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });





    });

    // this.document.on(app.resizeEventName, () => {
    // });

  },

};
app.init();