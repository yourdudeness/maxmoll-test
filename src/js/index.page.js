import 'jquery.global.js';

let mainPage = {

  init: function () {
    mainPage.app = this;

    this.document.ready(() => {
      mainPage.initIndex();
    });
  },

  initIndex() {
    const scrllTop = document.querySelector('.js-top');


    function scrollToTop(duration) {
      if (document.scrollingElement.scrollTop === 0) return;

      const totalScrollDistance = document.scrollingElement.scrollTop;
      let scrollY = totalScrollDistance, oldTimestamp = null;

      function step(newTimestamp) {
        if (oldTimestamp !== null) {
          scrollY -= totalScrollDistance * (newTimestamp - oldTimestamp) / duration;
          if (scrollY <= 0) return document.scrollingElement.scrollTop = 0;
          document.scrollingElement.scrollTop = scrollY;
        }
        oldTimestamp = newTimestamp;
        window.requestAnimationFrame(step);
      }
      window.requestAnimationFrame(step);
    }


    scrllTop.addEventListener('click', () => {
      scrollToTop(200)
    })


    const tabsContent = document.querySelectorAll('.js-tabs-content'),
      tabsItem = document.querySelectorAll('.js-tabs-link'),
      tabsParent = document.querySelector('.js-tabs-list');

    function hideTabs() {
      tabsContent.forEach((item) => {
        item.classList.remove('show');
        item.classList.add('hide');
      });

      tabsItem.forEach((item) => {
        item.classList.remove('active');
      })
    }

    function showTabs(i = 0) {
      tabsContent[i].classList.remove('hide');
      tabsContent[i].classList.add('show');
      tabsItem[i].classList.add('active');
    }

    hideTabs();
    showTabs();

    tabsParent.addEventListener('click', (event) => {
      const target = event.target;

      if (target && target.classList.contains('tabs-link')) {
        tabsItem.forEach((event, item) => {
          if (target == event) {
            hideTabs();
            showTabs(item);
          }
        })
      }
    })

    function numList(list) {
      let test = document.querySelectorAll(list);
      test.forEach((item, index) => {
        let counter = item.querySelector('.num');
        let counteText = '0' + (index + 1);
        counter.innerHTML = counteText.substr(-2, 2);

      })
    }

    numList('.js-list1');
    numList('.js-list2');
  }

};

export default mainPage;